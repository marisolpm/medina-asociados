<?php

use Illuminate\Support\Facades\Auth;

Auth::routes();

Route::get('/', 'Admin\DashboardController@index')->name('home');

//Grupos
Route::get('grupos/listado', 'AdminSistema\GruposController@listado');
Route::resource('grupos', 'AdminSistema\GruposController');

//Opciones
Route::get('opciones/listado', 'AdminSistema\OpcionesController@listado');
Route::resource('opciones', 'AdminSistema\OpcionesController');

//Roles
Route::get('roles/listado', 'AdminSistema\RolesController@listado');
Route::resource('roles', 'AdminSistema\RolesController');

//Accesos
Route::get('listaAc/{id_r}', 'AdminSistema\AsignacionController@listaAc');
Route::get('listaOp/{id_r}', 'AdminSistema\AsignacionController@listaOp');
Route::get('opciones/listado', 'AdminSistema\OpcionesController@listado');
Route::resource('accesos', 'AdminSistema\AccesosController');

//Usuarios

Route::get('usuarios/listado', 'AdminSistema\UsuariosController@listado');
Route::post('usuarios/{id}', 'AdminSistema\UsuariosController@actualizarUsuario');
Route::resource('usuarios', 'AdminSistema\UsuariosController');

//UsuariosRoles
Route::get('listaU', 'AdminSistema\UsuariosRolesController@listaU');
Route::get('listaR/{id_u}', 'AdminSistema\UsuariosRolesController@listaR');
Route::get('listaRno/{id_u}', 'AdminSistema\UsuariosRolesController@listaRno');
Route::get('usuariosRoles/listado', 'AdminSistema\UsuariosRolesController@listado');
Route::post('actualiza/{id_u}', 'AdminSistema\UsuariosRolesController@actualiza');
Route::post('actualiza1/{id_u}', 'AdminSistema\UsuariosRolesController@actualiza1');
Route::post('actualiza1', 'AdminSistema\AsignacionController@actualiza1');
Route::post('actualiza2', 'AdminSistema\AsignacionController@actualiza2');
Route::resource('usuariosRoles', 'AdminSistema\UsuariosRolesController');

//Perfiles

Route::post('subirFoto','AdminSistema\PerfilesController@subirFoto');
Route::post('rotarFoto','AdminSistema\PerfilesController@rotarFoto');
Route::resource('perfil', 'AdminSistema\PerfilesController');

//Resetar contraseña
Route::post('resetearContrasenaMail', 'Auth\LoginController@resetearContrasenaMail');
Route::post('resetearContrasena', 'Auth\LoginController@resetearContrasena');


//Clientes
Route::post('personaVerificaCI','Admin\ClientesController@verificaCI');
Route::get('listaClientes/{id_persona}/{id_tipo}', 'Admin\ClientesController@ListaPersonas');
Route::resource('clientes', 'Admin\ClientesController');

//Empleados
Route::get('listaEmpleados/{id_persona}/{id_tipo}', 'Admin\EmpleadosController@ListaPersonas');
Route::resource('empleados', 'Admin\EmpleadosController');

//AbogadoExpediente
Route::get('expedientesAbogadoListado/{id_expediente}/{id_abogado}', 'Admin\AbogadosExpedientesController@ListaAbogadosExpedientes');
Route::resource('abogados', 'Admin\AbogadosExpedientesController');

//Expedientes
Route::post('expedienteVerificaEXP','Admin\ExpedientesController@verificaEXP');
Route::get('expedientesOCR', 'Admin\ExpedientesController@ExpedientesOCR');
Route::get('expedientesListado/{id_expediente}/{id_cliente}/{posicion}', 'Admin\ExpedientesController@ListaExpedientes');
Route::get('expedientesListadoDocumentos/{id_expediente}/{id_expediente_doc}', 'Admin\ExpedientesController@ListaExpepdientesDocumentos');
Route::post('registrarRespaldoExp', 'Admin\ExpedientesController@registrarRespaldo');
Route::get('eliminarRespaldoExp/{id_exp_doc}', 'Admin\ExpedientesController@eliminarRespaldo');
Route::resource('expedientes', 'Admin\ExpedientesController');

//Expedientes Digitalizados
Route::post('extraeTextoP', 'Admin\ExpedientesDigitalizadosController@ProcesoExtraeTexto');
Route::post('extraeTexto', 'Admin\ExpedientesDigitalizadosController@ExtraeTexto');
Route::get('expedientesDListado/{id_expediente}/{id_liente}', 'Admin\ExpedientesDigitalizadosController@ListaExpedientes');
Route::get('verExpedientesDigitalizado/{id_expediente}', 'Admin\ExpedientesDigitalizadosController@verDigitalizado');
Route::post('subirWord', 'Admin\ExpedientesDigitalizadosController@subirWord');
Route::resource('digitalizacion', 'Admin\ExpedientesDigitalizadosController');


//Personas
Route::get('personas/{id_persona}/{id_tipo}', 'Admin\ClientesController@ListaPersonas');
Route::get('departamentos/{id_departamento}', 'Admin\ClientesController@ListaDepartamentos');

//Procesos
Route::get('procesosListado/{id_proceso}/{id_cliente}', 'Admin\ProcesosController@ListaProcesos');
Route::get('procesosListadoDocumentos/{id_proceso}/{id_proceso_doc}', 'Admin\ProcesosController@ListaProcesosDocumentos');
Route::get('procesosPosicion/{id_posicion}', 'Admin\ProcesosController@ListaProcesosPosicion');
Route::post('registrarRespaldo', 'Admin\ProcesosController@registrarRespaldo');
Route::get('eliminarRespaldo/{id_proc_doc}', 'Admin\ProcesosController@eliminarRespaldo');
Route::get('archivarProcesos/{id_proceso}', 'Admin\ProcesosController@archivarProceso');
Route::resource('procesos', 'Admin\ProcesosController');

//Tipos Proceso
Route::get('tiposProcesoListado', 'Admin\TiposProcesoController@ListaTiposProceso');
Route::resource('tiposProceso', 'Admin\TiposProcesoController');

//Estados Proceso
Route::get('estadosProcesoListado', 'Admin\EstadosProcesoController@ListaEstadosProceso');
Route::resource('estadosProceso', 'Admin\EstadosProcesoController');

//Seguimiento Economico
Route::get('datosCuenta', 'Admin\CuentasController@datosCuenta');
Route::get('cuentasListado/{id_cuenta}/{id_cliente}', 'Admin\CuentasController@ListaCuentas');
Route::resource('cuentas', 'Admin\CuentasController');

Route::get('datosRecibo', 'Admin\RecibosController@datosRecibo');
Route::get('recibosListado/{id_recibo}/{id_cliente}', 'Admin\RecibosController@ListaRecibos');
Route::get('imprimirRecibo/{id_recibo}', 'Admin\RecibosController@imprimirRecibo');
Route::resource('recibos', 'Admin\RecibosController');

//Citas
Route::get('citasListadoCalendario/{id_cita}/{id_cliente}', 'Admin\CitasController@ListaCitasCalendario');
Route::get('citasListado/{id_cita}/{id_cliente}', 'Admin\CitasController@ListaCitas');
Route::resource('citas', 'Admin\CitasController');

//Mensajeria
Route::get('mensajesListado/{id_mensaje}/{id_cliente}/{id_abogado}', 'Admin\MensajeriaController@ListaMensajes');
Route::resource('mensajeria', 'Admin\MensajeriaController');

//Plantilla
Route::get('plantillasListado/{id_plantilla}/{id_usuario}/{estado}', 'Admin\PlantillasController@ListaPlantillas');
Route::resource('plantillas', 'Admin\PlantillasController');

//Enlace
Route::get('enlacesListado/{id_enlace}/{id_usuario}/{estado}', 'Admin\EnlacesController@ListaEnlaces');
Route::resource('enlaces', 'Admin\EnlacesController');

Route::get('lenguaje','Admin\MenuController@lenguaje');
