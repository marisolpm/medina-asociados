<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myCreate" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                    ×
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Registrar Nuevo Grupo
                </h4>
            </div>
            <form 'id'='grupo'>
                <div class="modal-body">

                    <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="nombre" style="clear:both;">Nombre</label>
                        <input type="text" placeholder = 'Nombre de Grupo' class = 'form-control' id="grupo" >
                    </div>
                    <div class="form-group">
                        <label for="ruta" style="clear:both;">Ruta Imagen</label>
                        <input type = 'text' placeholder = 'Ruta de la imagen' class = 'form-control' id="imagen">
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        Cerrar
                    </button>
                    <a title='Registrar' id='registro' class='btn btn-primary'> Registrar</a>
                </div>
            </form>
        </div>
    </div>
</div>

