@extends('layouts.partials.main')
@section('content')
@include('admin.sistema.grupos.partials.modalCreate')
@include('admin.sistema.grupos.partials.modalUpdate')
<section class="content-header">
    <h1>
        <?php echo 'Grupos' ?>
        <small><?php echo 'Listado' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> <?php echo 'Dashboard' ?></a></li>
        <li class="active"><?php echo 'Grupos' ?></li>
    </ol>
</section>
<section class="content">
    <div class="row">

        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Grupos</h3>                                    
                </div><!-- /.box-header -->
                <div class="col-xs-12">
                    <div class="btn-group pull-right">
                        <button class="btn btn-primary fa fa-plus-square pull-right" data-target="#myCreate" data-toggle="modal" >&nbsp;Nuevo</button>
                    </div>
                </div>
                <div class="box-body table-responsive" style="margin-top:40px;">

                    <table class="table table-hover table-bordered" id="lts-grupo">
                        <thead class="cf">
                            <tr>
                                <th>Grupo</th>
                                <th>Imagen</th>
                                <th>Fecha Registro</th>
                                <th>Usuario Registro</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                    </table>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

</section>



<script>
    function Mostrar(id) {
        $('#myUpdate').modal();
        var route = "/grupos/" + id + "/edit";
        $.get(route, function (res) {
            $("#grupo1").val(res.grupo);
            $("#imagen1").val(res.imagen);
            $("#id").val(res.id);
        });
    }
    function Eliminar(id) {
        var route = "/grupos/" + id;
        var token = $("#token").val();
        swal({title: "Esta seguro de eliminar el Grupo?",
            text: "Presione ok para eliminar el registro de la base de datos!",
            type: "warning", showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar!",
            closeOnConfirm: false},
                function () {
                    $.ajax({
                        url: route,
                        headers: {'X-CSRF-TOKEN': token},
                        type: 'DELETE',
                        dataType: 'json',
                        success: function (data) {
                            $('#lts-grupo').DataTable().ajax.reload();
                            swal("Grupo!", "Fue eliminado correctamente!", "success");
                        },
                        error: function (result) {
                            swal("Opss..!", "El Grupo tiene registros en otras tablas!", "error")
                        }
                    });
                });
    }
    $(function () {
        $('#lts-grupo').DataTable({
            "serverSide": true,
            "destroy": true,
            "responsive": true,
            "processing": true,
            "ajax": "/grupos/listado",
            "columns": [
                {data: 'grupo', orderable: false},
                {data: 'imagen'},
                {data: 'registrado'},
                {data: 'usuario'},
                {data: 'acciones', searchable: false},
            ],
            "language": {
                "url": "/lenguaje"
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });




        $("#registro").click(function () {
            var route = "/grupos";
            var token = $("#token").val();
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data: {'grupo': $("#grupo").val(), 'imagen': $("#imagen").val()},
                success: function (data) {
                    $("#myCreate").modal('toggle');
                    swal("El Grupo!", "Fue registrado correctamente!", "success");
                    $('#lts-grupo').DataTable().ajax.reload();
                },
                error: function (result) {
                    swal("Opss..!", "Succedio un problema al registrar inserte bien los datos!", "error");
                }
            });
        });

        $("#actualizar").click(function () {
            var value = $("#id").val();
            var route = "/grupos/" + value + "";
            var token = $("#token").val();
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'PUT',
                dataType: 'json',
                data: {'grupo': $("#grupo1").val(), 'imagen': $("#imagen1").val()},
                success: function (data) {
                    $("#myUpdate").modal('toggle');
                    swal("El Grupo!", "Fue actualizado correctamente!", "success");
                    $('#lts-grupo').DataTable().ajax.reload();
                }, error: function (result) {
                    console.log(result);
                    swal("Opss..!", "El Grupo no se puedo actualizar intente de nuevo!", "error")
                }
            });
        });


    });



</script>
@endsection