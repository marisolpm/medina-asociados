<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myUserUpdate" tabindex="-5">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                    ×
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Modificar Usuario
                </h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                                <input id="id_usuario" name="id_usuario" type="hidden" value="">
                                <div class="form-group">
                                    <label class="control-label">
                                        Nombre:
                                    </label>
                                    <input type="text" name="nombre" id="nombre" class="form-control" disabled>
                                    
                                </div>
                                <div class="form-group">
                                    <label class="control-label">
                                        Nombre usuario:
                                    </label>
                                    <input type="text" name="nombre_usuario" id="nombre_usuario" class="form-control" >
                                    
                                </div>

                            </div>
                        </div>
                        </input>
                        </input>
                    </div>
                    <div class="modal-footer">
                        <a title='Modificar' class='btn btn-primary' id='actualizar'> Modificar</a>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cerrar</button>
                         
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
