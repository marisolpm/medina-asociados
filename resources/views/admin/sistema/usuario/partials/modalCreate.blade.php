<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myUserCreate" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">
                    Registrar Usuario
                </h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['id'=>'regUser'])!!}
                <div class="caption">

                    <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                        <input id="id" name="usr_id" type="hidden" value="">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Nombre:
                                            </label>
                                            {!! Form::text('usr_usuario', null, array('placeholder' => 'Nombre de Opcion','class' => 'form-control','name'=>'usr_usuario','id'=>'usuarioa')) !!}
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">
                                                Contraseña:
                                            </label>
                                            <input type="password" name="usr_clave" id="clavea" >
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Personas:</label>
                                            <div id="htmlcomboPersonal" ></div>
                                            <!--{!! Form::select('prs_id', $persona, null,['class'=>'form-control','name'=>'usr_prs_id',  'style'=>'width: 550px', 'id'=>'usr_prs_id']) !!}-->
                                        </div>
                                    </input>
                                </div>
                            </div>
                        </input>
                    </input>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">Cerrar</button>
                    {!!link_to('#',$title='Registrar', $attributes=['id'=>'registroUsuario','class'=>'btn btn-primary'], $secure=null)!!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
