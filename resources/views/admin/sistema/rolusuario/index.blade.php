@extends('layouts.partials.main')
@section('content')

<script type="text/javascript">

</script>
<section class="content-header">
    <h1>
        <?php echo 'Usuarios - Roles' ?>
        <small><?php echo 'Listado' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> <?php echo 'Dashboard' ?></a></li>
        <li class="active"><?php echo 'Usuarios - Roles' ?></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <form id="formulario">
            <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Usuarios</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table class="table table-hover table-striped" id="lts-usuario">
                                    <thead class="cf">
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Seleccionar</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Roles asignados</h3>
                            </div>
                            <div class="box-body">
                                <table class="table table-hover table-striped" id="lts-rolA">
                                    <thead class="cf">
                                        <tr>
                                            <th>Seleccionar</th>
                                            <th>ID</th>
                                            <th>Usuario</th>
                                            <th>Rol</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div style="padding-bottom: 20px;">
                            <a  class="btn btn-success" onclick="actualiza()"><i class="fa fa-chevron-up" ></i> Asignar</a>
                            <a  class="btn btn-danger" onclick="actualiza1()"><i class="fa fa-chevron-down" ></i> Designar</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Roles no asignados</h3>
                            </div>
                            <div class="box-body">
                                <table class="table table-hover table-striped" id="lts-no">
                                    <thead class="cf">
                                        <tr>
                                            <th>Seleccionar</th>
                                            <th>ID</th>
                                            <th>Rol</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    var id_r = "";
    function listar(va) {
        id_r = va;
        var url = "listaR/" + id_r;

        $('#lts-rolA').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: url,
            columns: [
                {data: 'rolasignado'},
                {data: 'rls_id'},
                {data: 'usr_usuario'},
                {data: 'rls_rol'},
            ],

            "language": {
                "url": "/lenguaje"
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
        $('#lts-no').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: "listaRno/" + id_r,
            columns: [
                {data: 'rolnoasignado'},
                {data: 'rls_id'},
                {data: 'rls_rol'},
            ],

            "language": {
                "url": "/lenguaje"
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });

    }
    function actualiza() {
        var url = "actualiza/" + id_r;
        var token = $("#token").val();
        console.log(id_r);
        $.ajax({
            type: 'POST',
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            data: $('#formulario').serialize(),
            success: function (data) {

                listar(id_r);
            },
            error: function (result) {
                console.log(result);
            }
        });
    }
    function actualiza1() {
        var url = "actualiza1/" + id_r;
        var token = $("#token").val();
        console.log(token);
        $.ajax({
            type: 'POST',
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            data: $('#formulario').serialize(),
            success: function (data) {

                listar(id_r);
            },
            error: function (result) {
                console.log(result);
            }
        });
    }
    $(function () {
        $('#lts-usuario').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "listaU",
            "columns": [
                {data: 'id'},
                {data: 'usuario'},
                {data: 'acciones'}
            ],
            "language": {
                "url": "/lenguaje"
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });

        $('#lts-rolA').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "listaR/" + 1,
            "columns": [
                {data: 'rolasignado'},
                {data: 'rls_id'},
                {data: 'usr_usuario'},
                {data: 'rls_rol'},
            ],
            "language": {
                "url": "/lenguaje"
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });

        $('#lts-no').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "listaRno/" + 1,
            "columns": [
                {data: 'rolnoasignado'},
                {data: 'rls_id'},
                {data: 'rls_rol'},
            ],
            "language": {
                "url": "/lenguaje"
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });





    });

</script>
@endsection