<div class="modal fade modal-primary" id="myCreate" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Registrar Rol</h4>
			</div>
                    <form id='rolss'>
			<div class="modal-body">
					
				<input type="hidden" name="csrf-token" value="{{ csrf_token() }}" id="token">
				<div class="form-group">
				
					 <label for="grupo" style="clear:both;">Rol</label>
					<input placeholder = 'Nombre de Rol' class = 'form-control' id= 'rls_rol'>
				</div>
			</div>
			<div class="modal-footer">
	 			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <a title='Registrar'  id='registro' class='btn btn-primary'>Registrar </a>
				
			</div>
                    </form>
		</div>
	</div>
</div>