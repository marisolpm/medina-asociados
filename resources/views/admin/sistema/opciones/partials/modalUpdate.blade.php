<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myUpdate" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">
                    Modificar Opción
                </h4>
            </div>
            <form id='opciones1'>
                <div class="modal-body">

                    <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                    <input id="id"  type="hidden">
                    <div class="form-group">
                        <label for="grupo" style="clear:both;">Grupo</label>
                        <select class='form-control' name='grp_id1' id='grpid1'>
                            @foreach($grupos as $grupo)
                            <option value="{{$grupo->id}}">{{$grupo->grupo}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="opcion" style="clear:both;">Opción</label>
                        <input type = 'text' placeholder = 'Nombre de la opcion' class ='form-control' id='opcion1'>
                    </div>
                    <div class="form-group">
                        <label for="clntenido" style="clear:both;">Contenido</label>
                        <input type = 'text' placeholder ='Contenido / Detalle' class = 'form-control' id='contenido1'>
                    </div>
                    </input>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">Cerrar</button>
                    <a title='Actualizar' id='actualizar' class='btn btn-primary'>Registrar</a>

                </div>
            </form>
        </div>
    </div>
</div>