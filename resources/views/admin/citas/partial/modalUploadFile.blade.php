<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="modal-event-up">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Actualizar Cita </h4>
            </div>
            <div class="panel-body">
                <form action="" method="POST" id="formularioCitaUp" >
                    <input id="token_registrar" name="token_registrar_up" type="hidden" value="{{ csrf_token() }}"/>

                    <input id="id_cita" name="id_cita_up" type="hidden" value="-1"/>    
                    <input id="tipo_registro" name="tipo_registro" type="hidden" value="1"/>
                    <div class="col-md-4">
                        <label for="cliente" style="clear:both;">Cliente</label>
                        <select name="cliente" id="cliente" class="form-control" onchange="procesosCliente(this)">

                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="inputEmail1" class="control-label">Asunto</label>
                        <input type="text" name="asunto" id="asunto" required class="form-control" placeholder="Asunto">

                    </div>

                    <div class="col-md-4">
                        <label for="inputEmail1" class="control-label">Abogado</label>

                        <select name="empleado" id="empleado" class="form-control" required>


                        </select>

                    </div>
                    <div class="col-md-4">
                        <label for="inputEmail1" class="control-label">Lugar Cita</label>
                        <input type="text" name="lugar" id="lugar" required class="form-control" placeholder="Lugar">
                    </div>
                    <div class="col-md-4">
                        <label for="inputEmail1" class="control-label">Fecha</label>
                        <input type="date" name="fecha" id="fecha" required class="form-control" placeholder="Fecha">
                    </div>
                    <div class="col-md-4">
                        <label for="inputEmail1" class="control-label">Hora</label>
                        <input type="time" name="hora" id="hora" required class="form-control" placeholder="Hora">
                    </div>
                    <div class="col-md-4">
                        <label for="inputEmail1" class="control-label">Estado de la cita</label>
                        <select name="estado_cita" id="estado_cita" class="form-control" required>
                            <option value="1">Pendiente</option>
                            <option value="2">Realizada</option>
                            <option value="1">No asistió</option>
                            <option value="1">Cancelada</option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <label for="inputEmail1" class="control-label">Descripción Cita</label>
                        <textarea class="form-control" name="descripcion" id="descripcion" placeholder="Descripción"></textarea>
                    </div>

                </form>

            </div>
            
            <div class="modal-footer">
                 <a  class="btn btn-success"  id="registrar_cita">REGISTRAR</a>
                <a  class="btn btn-primary" onclick="limpiar()">LIMPIAR</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->