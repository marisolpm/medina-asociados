@extends('layouts.partials.main')
@section('content')

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

</script>
<section class="content-header">
    <h1>
        <?php echo 'Empleados' ?>
        <small><?php echo 'Listado' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"> Empleados</li>
    </ol>
</section>

<section class="content">
    <?php
    if (false) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
            <b><?php echo 'alert'; ?>!</b><?php echo 'hola'; ?>
        </div>

    <?php } ?>
    <div class="row" style="margin-bottom:10px;">
        <div class="col-xs-12">
            <div class="btn-group pull-right">
                    <a class="btn btn-default" href="empleados/create"><i class="fa fa-plus"></i> <?php echo 'Nuevo' ?></a>
            </div>
        </div>    
    </div>	

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo 'Empleados' ?></h3>   
                    <input id="token_registrar" name="token_registrar" type="hidden" value="{{ csrf_token() }}"/>
                </div><!-- /.box-header -->

                <div class="box-body table-responsive" style="margin-top:40px;">

                    <table id="lts_empleados" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Numero</th>
                                <th>Nombre</th>
                                <th>Área</th>
                                <th>Matrícula</th>
                                <th>Celular</th>
                                <th>Rol</th>
                                <th>Modificado</th>
                                <th>Usuario</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>

                    </table>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>

    </div>
</section>


<script type="text/javascript">
    $(function () {
        $(function () {
            //bootstrap WYSIHTML5 - text editor
            $(".txtarea").wysihtml5();
        });

        jQuery('.datepicker').datetimepicker({
            lang: 'es',
            i18n: {
                de: {
                    months: [
                        'Enero', 'Febrero', 'Marzo', 'Abril',
                        'Mayo', 'Junio', 'Julio', 'Agosto',
                        'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',
                    ],
                    dayOfWeek: [
                        "Do.", "Lu", "Ma", "Mi",
                        "Ju", "Vi", "Sa.",
                    ]
                }
            },
            timepicker: false,
            format: 'd/M/Y'
        });

        $("#lts_empleados").DataTable({
            processing: true,
            serverSide: true,
            ajax: 'listaEmpleados/-1/1',
            "dom": 'Bfrtip',
            "buttons": ['pageLength',
                {
                    extend: 'excelHtml5',
                    title: 'REPORTE EMPLEADOS',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    title: 'REPORTE EMPLEADOS',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    },
                    orientation: 'landscape',
                    pageSize: 'Letter',
                    download: 'open',
                    filename: 'REPORTE EMPLEADOS',
                    alignment: 'center'
                }
            ],
            columns: [
                {data: "o_prs_id", orderable: false},
                {data: "o_prs_nombre_completo"},
                {data: "o_prs_area"},
                {data: "o_prs_matricula"},
                {data: "o_prs_contacto"},
                {data: "o_prs_rol"},
                {data: "o_prs_modificado"},
                {data: "o_prs_usr_mod"},
                {data: "o_accion"}

            ], fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                $('td', nRow).css('background-color', '#FFFFFF');
            },
            "language": {
                "url": "/lenguaje"
            },
            "lengthMenu": [[20, 25, 50, -1], [20, 25, 50, "All"]]
        });


    });

    function eliminarEmpleado(id) {

        var route = "/empleados/" + id;
        var token = $("#token_registrar").val();
        swal({
            title: "¿ELIMINAR EMPLEADO?",
            text: "Revise datos antes de proceder.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, ELIMINAR!",
            cancelButtonText: "No, revisar",
            closeOnConfirm: true,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
                function (isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'DELETE',
                            success: function (data) {
                                swal("EMPLEADO", "FUE ELIMINADO DEL REGISTRO", "success");
                                $('#lts_empleados').DataTable().ajax.reload();
                            }, error: function (result) {
                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                            }
                        });
                    } else {
                        return false;
                    }
                });
    }


</script>
@endsection