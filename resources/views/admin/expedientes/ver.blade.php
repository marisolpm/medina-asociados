@extends('layouts.partials.main')
@section('content')
<!-- Content Header (Page header) -->
<link href="<?php echo asset('assets/css/chosen.css') ?>" rel="stylesheet" type="text/css" />
<style>
    .row{
        margin-bottom:10px;
    }

    .chosen-container { width: 100% !important; }
</style>
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>
        <?php echo 'Expedientes' ?>
        <small><?php echo 'Ver' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i><?php echo 'Dashboard' ?></a></li>
        <li><a href="/expedientes">Expedientes</a></li>
        <li class="active"><?php echo 'Ver' ?></li>
    </ol>
</section>

<section class="content" >
    <div class="row">
        <!-- left column -->

        <!-- general form elements -->
        <div class="box box-primary col-md-12">
            <div class="box-header">
                <h3 class="box-title">{{$clientes[0]->o_prs_nombre_completo}}</h3>
            </div><!-- /.box-header -->
            <ul class="nav nav-tabs">
                <li class="active" id="tabLista">
                    <a data-toggle="tab" href="#lista">
                        <i class="fa fa-folder"></i> <span>Lista Expedientes</span>

                    </a>
                </li>
                <li class="" id="tabDetalle">
                    <a data-toggle="tab" href="#detalle">
                        <i class="fa fa-eye"></i> <span>Detalle Expediente (Abogados Asignados)</span>

                    </a>
                </li>
                <li class="" id="tabDocumento">
                    <a data-toggle="tab" href="#documentos">
                        <i class="fa fa-file"></i> <span>Detalle Expediente (Documentos)</span>

                    </a>
                </li>
                <li  class="left" id="tabNuevo">
                    <a data-toggle="tab" href="#nuevo" onclick="resetear()">
                        <i class="fa fa-plus-square"></i> <span>Nuevo/Editar Expediente</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <!--///////////   inicio    /////////////////////    -->
                <div class="tab-pane active" id="lista">
                    <div class="panel-body table-responsive">

                        <div class="table-responsive" style="margin-top:10px;">
                            <table class="table table-bordered table-striped" id="lts_expedientes">
                                <thead class="cf">
                                    <tr>
                                        <th>ID</th>
                                        <th>Expediente N°</th>
                                        <th>Demandante</th>
                                        <th>Demandado</th>
                                        <th>Estado</th>
                                        <th>Fecha Registro</th>
                                        <th>Usuario Registro</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                        <input type="hidden" id="dp_id" >
                    </div>
                </div>
                <div class="tab-pane fade" id="detalle">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detalle Expediente</h3>

                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Código Interno</th><td>&nbsp;</td><td id="codigoE"></td></tr>
                                            <tr><th>Expediente N°</th><td>&nbsp;</td><td id="expedienteE"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Materia</th><td>&nbsp;</td><td id="materiaE"></td></tr>
                                            <tr><th>Estado</th><td>&nbsp;</td><td id="estadoE"></td></tr>
                                            <tr><th>Contingencia</th><td>&nbsp;</td><td id="contingenciaE"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Fecha registro</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="registroE"></span></td></tr>
                                            <tr><th>Última modificación</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="modificadoE"></span></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Partes procesales</h4>
                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Demandante</th><td>&nbsp;</td><td id="demandanteE"></td></tr>
                                            <tr><th>Demandado</th><td>&nbsp;</td><td id="demandadoE"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <h4>Abogados asignados</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="lts_abogados_asignados">
                                        <thead class="cf">
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Paterno</th>
                                                <th>Materno</th>
                                                <th>Perfíl</th>
                                                <th>Fecha Asignación</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Asignar un abogado al caso</h4>
                                <hr>
                                <h6><b>Nota:</b> Seleccione los abogados que estaran a cargo del caso. si desea puede retirarlo
                                    haciendo clic en el aspa "X" luego debe pulsar los cambios en el boton <b>"Asignar & Guardar"</b></h6>
                            </div>
                            <form enctype="multipart/form-data" method="POST" id="formulario_asignacion_abogado">
                                <input id="token_registrar_abogado" name="token_registrar_abogado" type="hidden" value="{{ csrf_token() }}"/>
                                <input id="expediente" name="expediente" type="hidden" value=""/>
                                <input id="tipo_registro_abogado" name="tipo_registro_abogado" type="hidden" value="1"/>
                                <div class="col-md-8">
                                    <select name="abogados[]"  class="chzn form-control" style="width: 100%" multiple="multiple" data-placeholder="Asigne Abogados" >
                                        @foreach($empleados as $empleado)
                                        <option value="{{$empleado->o_prs_id}}">{{$empleado->o_prs_nombre_completo}}</option>
                                        @endforeach 
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <a  class="btn btn-primary" id="registrar_abogado_expediente"><i class="fa fa-plus-circle"></i> Asignar & Guardar</a>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>

                <div class="tab-pane fade" id="documentos">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detalle Documentos</h3>

                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Código Interno</th><td>&nbsp;</td><td id="codigoE"></td></tr>
                                            <tr><th>Expediente N°</th><td>&nbsp;</td><td id="expedienteE"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Materia</th><td>&nbsp;</td><td id="materiaE"></td></tr>
                                            <tr><th>Estado</th><td>&nbsp;</td><td id="estadoE"></td></tr>
                                            <tr><th>Contingencia</th><td>&nbsp;</td><td id="contingenciaE"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Fecha registro</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="registroE"></span></td></tr>
                                            <tr><th>Última modificación</th><td>&nbsp;</td><td ><i class="fa fa-clock-o"></i> <span id="modificadoE"></span></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Partes procesales</h4>
                                <div class="table-responsive">
                                    <table>
                                        <thead>
                                            <tr><th>Demandante</th><td>&nbsp;</td><td id="demandanteE"></td></tr>
                                            <tr><th>Demandado</th><td>&nbsp;</td><td id="demandadoE"></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <h4>Abogados asignados</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="lts_abogados_asignados">
                                        <thead class="cf">
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Paterno</th>
                                                <th>Materno</th>
                                                <th>Perfíl</th>
                                                <th>Fecha Asignación</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Asignar un abogado al caso</h4>
                                <hr>
                                <h6><b>Nota:</b> Seleccione los abogados que estaran a cargo del caso. si desea puede retirarlo
                                    haciendo clic en el aspa "X" luego debe pulsar los cambios en el boton <b>"Asignar & Guardar"</b></h6>
                            </div>
                            <form enctype="multipart/form-data" method="POST" id="formulario_asignacion_abogado">
                                <input id="token_registrar_abogado" name="token_registrar_abogado" type="hidden" value="{{ csrf_token() }}"/>
                                <input id="expediente" name="expediente" type="hidden" value=""/>
                                <input id="tipo_registro_abogado" name="tipo_registro_abogado" type="hidden" value="1"/>
                                <div class="col-md-8">
                                    <select name="abogados[]"  class="chzn form-control" style="width: 100%" multiple="multiple" data-placeholder="Asigne Abogados" >
                                        @foreach($empleados as $empleado)
                                        <option value="{{$empleado->o_prs_id}}">{{$empleado->o_prs_nombre_completo}}</option>
                                        @endforeach 
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <a  class="btn btn-primary" id="registrar_abogado_expediente"><i class="fa fa-plus-circle"></i> Asignar & Guardar</a>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>

                <div class="tab-pane fade" id="nuevo">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">NUEVO/EDITAR EXPEDIENTE</h3>

                        </div>
                        <div class="panel-body">
                            <form action="" method="POST" id="formularioExpediente" enctype='multipart/form-data'>
                                <input id="token_registrar" name="token_registrar" type="hidden" value="{{ csrf_token() }}"/>
                                <input id="cliente" name="cliente" type="hidden" value="{{$clientes[0]->o_prs_id}}"/>    
                                <input id="expediente" name="expediente" type="hidden" value=""/>    
                                <input id="tipo_registro" name="tipo_registro" type="hidden" value=""/>    
                                <div class="col-md-3">
                                    <label for="codigo" style="clear:both;">Código de expediente</label>
                                    <input type="text" name="codigo" id="codigo" placeholder="Código" class="form-control" autocomplete="off">
                                </div>


                                <div class="col-md-3">
                                    <label for="numero" style="clear:both;">Expediente N°</label>
                                    <input type="text" name="numero" id="numero" placeholder="Expediente N°" class="form-control" autocomplete="off">
                                </div>

                                <div class="col-md-3">
                                    <label for="demandante" style="clear:both;">Demandante</label>
                                    <select name="demandante"  id="demandante" class="form-control" style="width: 100%">

                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="materia" style="clear:both;">Materia</label>
                                    <select name="materia" id="materia" class="form-control">
                                        <option value="0">Seleccione materia</option>
                                        <option value="1">Judicial</option>
                                        <option value="2">Administrativo</option>
                                        <option value="3">Policial</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="demandado" style="clear:both;">Demandado</label>
                                    <select name="demandado" id="demandado" class="form-control" style="width: 100%">

                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="estado" style="clear:both;">Estado</label>
                                    <select name="estado" id="estado" class="form-control">
                                        <option value="0">Seleccione estado</option>
                                        <option value="1">Postulatoria</option>
                                        <option value="2">Probatoria</option>
                                        <option value="3">Expositiva</option>
                                        <option value="4">Consultiva</option>
                                        <option value="5">Juicio</option>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label for="contingencia" style="clear:both;">Contingencia</label>
                                    <select name="contingencia" id="contingencia" class="form-control">
                                        <option value="0">Seleccione contingencia</option>
                                        <option value="1">Remoto</option>
                                        <option value="2">Probable</option>
                                        <option value="3">Eventual</option>
                                    </select>
                                </div>

                                <div class="col-md-12">
                                    <label for="observaciones" style="clear:both;">Observaciones/Comentario</label>
                                    <textarea name="observaciones" id="observaciones" class="form-control" placeholder="Observaciones/Comentario"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <a  class="btn btn-success"  id="registrar_expediente">REGISTRAR</a>
                                    <a  class="btn btn-primary" onclick="limpiar()">LIMPIAR</a>
                                    <a class="btn btn-danger" onclick="cancelar()" >CANCELAR</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  
<script src="<?php echo asset('assets/js/chosen.jquery.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo asset('assets/js/redactor.min.js') ?>"></script>
<script type="text/javascript">
                                        $(function () {

                                            $('.chzn').chosen();

                                        });
</script>
<script type="text/javascript">
    var id_cliente = $('#cliente').val();
    function cargando() {
        var texto = $("<div>", {
            text: "CARGANDO....",
            id: "myEstilo",
            css: {
                "font-size": "30px",
                "position": "relative",
                "width": "500px",
                "height": "300px",
                "left": "180px",
                "top": "50px"
            },
            fontawesome: "fa fa-spinner"
        });
        $.LoadingOverlay("show", {
            custom: texto,
            //fontawesome : "fa fa-spinner",
            color: "rgba(255, 255, 255, 0.8)",
        });
    }

    function resetear() {
        document.getElementById('formularioExpediente').reset();
        $('#tipo_registro').val(1);
    }
    function detalleExpediente(id)
    {
        $('.nav-tabs a[href="#detalle"]').tab('show');
        var path = '/expedientesListado/' + id + '/-1';
        $.get(path, function (res) {
            $('#expediente').val(id);

            document.getElementById("codigoE").innerHTML = res.data[0].o_id_expediente;
            document.getElementById("expedienteE").innerHTML = res.data[0].o_nro_expediente;
            document.getElementById("demandanteE").innerHTML = res.data[0].o_nombre_demandante;
            document.getElementById("demandadoE").innerHTML = res.data[0].o_nombre_demandado;
            document.getElementById("materiaE").innerHTML = res.data[0].o_materia;
            document.getElementById("estadoE").innerHTML = res.data[0].o_estado_expediente;
            document.getElementById("contingenciaE").innerHTML = res.data[0].o_contingencia;
            document.getElementById("registroE").innerHTML = res.data[0].o_registrado;
            document.getElementById("modificadoE").innerHTML = res.data[0].o_modificado;
            document.getElementById("observacionesE").innerHTML = res.data[0].o_observaciones;

        });

        $('#lts_abogados_asignados').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: '/expedientesAbogadoListado/' + id + '/-1',
            columns: [
                {data: "o_nombre", orderable: false},
                {data: "o_paterno"},
                {data: "o_materno"},
                {data: "o_perfil"},
                {data: "o_asignado"},
                {data: "o_accion"}
            ],

            "language": {
                "url": "/languaje"
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });

    }
    function mostrarExpediente(id)
    {
        $('.nav-tabs a[href="#nuevo"]').tab('show');
        $('#tipo_registro').val(2);
        var path = '/expedientesListado/' + id + '/-1';
        $.get(path, function (res) {
            $('#expediente').val(id);
            $('#codigo').val(res.data[0].o_id_expediente);
            $('#numero').val(res.data[0].o_nro_expediente);
            $('#demandante').val(res.data[0].o_demandante);
            $('#demandado').val(res.data[0].o_demandado);
            $('#materia').val(res.data[0].o_id_materia);
            $('#estado').val(res.data[0].o_id_estado_expediente);
            $('#contingencia').val(res.data[0].o_id_contingencia);
            $('#observaciones').val(res.data[0].o_observaciones);
        });

    }

    $(function () {
        $("#demandante").select2();
        $("#demandado").select2();
        $.ajax({
            url: '/personas/-1/2',
            type: 'GET',
            dataType: 'json',
            success: function (s)
            {
                $('#demandante').find('option').remove();
                $('#demandante').append('<option value=0>ELIGA UNA PERSONA</option>');
                $(s.data).each(function (k, z) { // indice, valor
                    $('#demandante').append('<option value= ' + z.o_prs_id + '>' + z.o_prs_nombre_completo + '</option>');
                });
                $('#demandado').find('option').remove();
                $('#demandado').append('<option value=0>ELIGA UNA PERSONA</option>');
                $(s.data).each(function (k, z) { // indice, valor
                    $('#demandado').append('<option value= ' + z.o_prs_id + '>' + z.o_prs_nombre_completo + '</option>');
                });
            },
            error: function ()
            {
                alert('Ocurrio un error en el servidor ..');
            }
        });



        $('#lts_expedientes').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: '/expedientesListado/-1/' + id_cliente,
            columns: [
                {data: "o_id_expediente", orderable: false},
                {data: "o_nro_expediente"},
                {data: "o_nombre_demandante"},
                {data: "o_nombre_demandado"},
                {data: "o_estado_expediente"},
                {data: "o_registrado"},
                {data: "o_usuario_mod"},
                {data: "accion"}
            ],

            "language": {
                "url": "/languaje"
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });

    });
    $("#registrar_expediente").click(function () {
        var tipo_registro = $('#tipo_registro').val();
        var token = $("#token_registrar").val();
        if (tipo_registro == 1) {
            var route = "/expedientes";

            swal({
                title: "¿Desea registrar el expediente?",
                text: "Se sugiere revise los datos antes de proceder",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Sí, REGISTRAR!",
                cancelButtonText: "No, revisar",
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true
            },
                    function (isConfirm) {
                        if (!isConfirm)
                            return;
                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'POST',
                            dataType: 'json',
                            data: $('#formularioExpediente').serialize(),
                            success: function (data) {

                                swal('EXPEDIENTE N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                $('.nav-tabs a[href="#lista"]').tab('show');
                                $('#lts_expedientes').DataTable().ajax.reload();
                                document.getElementById('formularioExpediente').reset();
                            }, error: function (result) {
                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                            }
                        });
                    });
        } else {
            var expediente = $('#expediente').val();
            var route = "/expedientes/" + expediente;

            swal({
                title: "¿Desea actualizar el expediente?",
                text: "Se sugiere revise los datos antes de proceder",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Sí, ACTUALIZAR!",
                cancelButtonText: "No, revisar",
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true
            },
                    function (isConfirm) {
                        if (!isConfirm)
                            return;
                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'PUT',
                            dataType: 'json',
                            data: $('#formularioExpediente').serialize(),
                            success: function (data) {

                                swal('EXPEDIENTE N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                $('.nav-tabs a[href="#lista"]').tab('show');
                                $('#lts_expedientes').DataTable().ajax.reload();
                                document.getElementById('formularioExpediente').reset();
                            }, error: function (result) {
                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                            }
                        });
                    });
        }
    });

    $("#registrar_abogado_expediente").click(function () {
        var tipo_registro = $('#tipo_registro_abogado').val();
        var token = $("#token_registrar_abogado").val();
        if (tipo_registro == 1) {
            var route = "/abogados";

            swal({
                title: "¿Desea asignar abogado?",
                text: "Se sugiere revise los datos antes de proceder",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Sí, ASIGNAR!",
                cancelButtonText: "No, revisar",
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true
            },
                    function (isConfirm) {
                        if (!isConfirm)
                            return;
                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'POST',
                            dataType: 'json',
                            data: $('#formulario_asignacion_abogado').serialize(),
                            success: function (data) {

                                swal('ABOGADO', data.err_mensaje, "success");
                                $('#lts_abogados_asignados').DataTable().ajax.reload();
                                document.getElementById('formulario_asignacion_abogado').reset();
                            }, error: function (result) {
                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                            }
                        });
                    });
        } else {
            var expediente = $('#expediente').val();
            var route = "/expedientes/" + expediente;

            swal({
                title: "¿Desea actualizar el expediente?",
                text: "Se sugiere revise los datos antes de proceder",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Sí, ACTUALIZAR!",
                cancelButtonText: "No, revisar",
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true
            },
                    function (isConfirm) {
                        if (!isConfirm)
                            return;
                        $.ajax({
                            url: route,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'PUT',
                            dataType: 'json',
                            data: $('#formularioExpediente').serialize(),
                            success: function (data) {

                                swal('EXPEDIENTE N°: ' + data.retorno_codigo, data.err_mensaje, "success");
                                $('.nav-tabs a[href="#lista"]').tab('show');
                                $('#lts_expedientes').DataTable().ajax.reload();
                                document.getElementById('formularioExpediente').reset();
                            }, error: function (result) {
                                swal("Opss..!", "Succedio un problema al registrar, inserte bien los datos!", "error");
                            }
                        });
                    });
        }
    });
</script>
@endsection