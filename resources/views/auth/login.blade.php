<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Medina & Asociados</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="assets/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="{{asset('assets/sweetalert/dist/sweetalert.css')}}" rel="stylesheet" type="text/css" />
        <link href="assets/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pickmeup.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- jQuery 2.0.2 -->
        <script src="assets/js/jquery.js"></script>
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <style>
        .html,body{
            /* Para que funcione correctamente en Smarthpones y Tablets */
            height:100vh;
        }
        body {
            /* Ruta relativa o completa a la imagen */
            background-image: url('assets/img/login-bg.jpg');
            /* Centramos el fondo horizontal y verticalmente */
            background-position: center center;
            /* El fonde no se repite */
            background-repeat: no-repeat;
            /* Fijamos la imagen a la ventana para que no supere el alto de la ventana */
            background-attachment: fixed;
            /* El fonde se re-escala automáticamente */
            background-size: cover;
            /* Color de fondo si la imagen no se encuentra o mientras se está cargando */
            background-color: #FFF;
            /* Fuente para el texto */
            text-align: center;
            color: #000;
            font-family: "Times New Roman", Times, serif;
        }
        /* Background para ancho máximo de la pantalla física */
        @media only screen and (max-device-width: 767px) {
            body {
                background-image: url('assets/img/login-bg.jpg');
                color: #FFF;
            }
        }
        /* Background para ancho máximo del navegador */
        @media only screen and (max-width: 767px) {
            body {
                background-image: url('assets/img/login-bg.jpg');
                color: #FFF;
            }
        }
        /* Dimensiones del div principal */
        #lorem {
            width: 90%;
            margin: auto;
            padding: 10px;
        }

    </style>
    <body>
        <div class="col-md-2"></div>
        <div class="form-box col-md-6" id="login-box">
            <div class="header">INICIAR SESIÓN</div>
            <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="body bg-gray">
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus >

                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required>
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>

                </div>
                <div class="footer">
                    <button type="submit" class="btn bg-olive btn-block">INGRESAR</button>
                    <p><a href = "/password/reset" >Olvide mi contraseña</a></p>
                </div>
            </form>
        </div>
        <div class="col-md-4" style="color: #FFF; font-size: 50px;left: 50%;
             position: absolute;
             top: 50%;

             transform: translate(0%, -50%);">MEDINA & ASOCIADOS</div>
        <!-- Modal -->
        <div class="modal fade bd-example-modal-xl" id="modal-event">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="" method="POST" id="formularioPasswordReset" >
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Resetear contraseña </h4>
                        </div>
                        <div class="panel-body">

                            <input id="token_resetear" name="token_resetear" type="hidden" value="{{ csrf_token() }}"/>
                            <div class="col-md-12">
                                <label for="inputEmail1" class="control-label">Correo electronico</label>
                                <input type="email" name="mail" id="mail" required class="form-control" placeholder="E-mail" autocomplete="off">
                            </div>

                        </div>

                        <div class="modal-footer">
                            <button type="button"  class="btn btn-success"  id="resetear_contrasena">ENVIAR</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </body>
    <script src="assets/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{asset('assets/sweetalert/dist/sweetalert.min.js')}}" type="text/javascript"></script>
    <script>
function resetearContrasena() {
    $('#modal-event').modal('show');
}

$("#resetear_contrasena").click(function () {

    var route = "resetearContrasena";
    var token = $("#token_resetear").val();
    swal({
        title: "¿Desea resetear contraseña?",
        text: "Revise datos antes de proceder.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "¡Sí, RESETEAR!",
        cancelButtonText: "No, revisar",
        closeOnConfirm: true,
        closeOnCancel: true,
        showLoaderOnConfirm: true
    },
            function (isConfirm) {
                if (isConfirm) {

                    $.ajax({
                        url: route,
                        headers: {'X-CSRF-TOKEN': token},
                        type: 'POST',
                        dataType: 'json',
                        data: $('#formularioPasswordReset').serialize(),
                        success: function (data) {
                            if(data[0].err_codigo === 0){
                                swal(data[0].retorno_codigo, data[0].err_mensaje, "success");
                                $('#modal-event').modal('hide');
                            }else{
                                swal(data[0].retorno_codigo, data[0].err_mensaje, "error");
                            }
                            

                        }, error: function (result) {
                            swal("Opss..!", "E-mail incorrecto, inserte bien los datos!", "error");
                        }
                    });
                } else {
                    return false;
                }
            });
});
    </script>
</html>
