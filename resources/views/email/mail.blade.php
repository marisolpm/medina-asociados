<table cellspacing="0" cellpadding="0" border="0" width="600">
    <tbody>
        <tr>
            <td height="2"></td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ffffff">
                    <tbody><tr>
                            <td height="24"><div></div></td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tbody><tr>
                                            <td width="30"><div></div></td>
                                            <td align="left">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tbody><tr>
                                                            <th width="20" style="padding:0;Margin:0;border:0;font-weight:normal;vertical-align:top" valign="top"><div></div></th>
                                                            <th align="left" style="padding:0;Margin:0;border:0;font-weight:normal;vertical-align:middle" valign="middle">
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colspan="2" align="left" style="line-height:1.2"><font style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-size:14px;text-transform:uppercase;color:#25282a;font-weight:normal;line-height:1.2">
                                                                                {{$clienteN}} <br> {{$abogadoN}}  </font></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="7" colspan="2"><div></div></td> </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="justify" style="line-height:1.2"><font style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-size:14px;text-transform:uppercase;color:#25282a;font-weight:normal;line-height:1.2">El estudio Jurídico <b>Medina & Asociados</b> tiene a bien informar lo siguiente:.<br><br >
                                                                                <table style="border-collapse: collapse; border: 0px solid black; font-size: 9px; text-align: left" width="100%">
                                                                                    <tr>
                                                                                        <td width="50%" align="left"  style="line-height:1.2"><font style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-size:12px;text-transform:uppercase;color:#25282a;font-weight:normal;line-height:1.2">
                                                                                                <b>ASUNTO: </b>{{$asunto}} <br>
                                                                                                <b>MENSAJE: </b>{{$mensaje}}
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>

                                                                                <br>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left"><font style="font-size:9.0pt; font-family:Levenim MT; color:#17365D">
                                                                                <br>
                                                                                <i>MEDINA & ASOCIADOS</i> <br>
                                                                                <i>ESTUDIO JURÍDICO</i><br>
                                                                                <i>LA PAZ - BOLIVIA</i>
                                                                                </font></td>
                                                                            <td align="center"><img src="https://images.app.goo.gl/3YD6odg1q5job3Ws9" width="100" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="20" colspan="2"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody></table>
            </td>
        </tr>
    </tbody>
</table>