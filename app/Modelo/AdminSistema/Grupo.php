<?php

namespace App\Modelo\AdminSistema;

use Illuminate\Database\Eloquent\Model;
use DB;

class Grupo extends Model {

    protected $table = 'acceso.grupos';
    protected $fillable = ['id', 'grupo', 'imagen', 'registrado', 'modificado', 'usr_id', 'estado'];
    public $timestamps = false;
    protected $primaryKey = 'id';

    protected static function getListar() {
        $grupillo = \DB::select('SELECT '
                        . 'grp.id,'
                        . 'grp.grupo,'
                        . 'grp.imagen,'
                        . "to_char(grp.registrado,'DD/MM/YYYY HH24:MI') as registrado,"
                        . 'users.usuario '
                        . 'FROM acceso.grupos grp JOIN users ON users.id = grp.usr_id WHERE grp.estado = ?', array('A'));
        return $grupillo;
    }

}
