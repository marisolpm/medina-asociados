<?php

namespace App\Modelo\AdminSistema;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;


class RolUsuario extends Model
{
   protected $table      = 'acceso.usuarios_roles';
    protected $fillable   = ['id', 'usr_id', 'rls_id', 'registrado', 'modificado', 'usuarios_usr_id', 'estado'];
    protected $primaryKey = 'id';
    public $timestamps    = false;
    protected static function getListar($vd)
    {
    	$usurol = \DB::select('select * from  acceso.lista_rno(?)',array($vd));
        return $usurol;
    }
    protected static function getListar1($row)
    {
    	$rolusuario = RolUsuario::join('_bp_roles as r', 'r.rls_id', '=', '_bp_usuarios_roles.usrls_rls_id')
                ->join('_bp_usuarios as u', 'u.usr_id', '=', '_bp_usuarios_roles.usrls_usr_id')
                ->select('_bp_usuarios_roles.usrls_id','r.rls_id','u.usr_usuario', 'r.rls_rol')
                ->where('r.rls_estado', '<>', 'B')
                ->where('u.usr_estado', '<>', 'B')
                ->where('_bp_usuarios_roles.usrls_usr_id', $row[0])
                ->where('_bp_usuarios_roles.usrls_estado', '<>', 'B')
                ->OrderBy('_bp_usuarios_roles.usrls_id', 'ASC')
                ->get();
        return $rolusuario;
    }


    public function consulta1()
    {
         $rolusuario = RolUsuario::join('_bp_roles as r', 'r.rls_id', '=', '_bp_usuarios_roles.usrls_rls_id', '')
                    ->join('_bp_usuarios as u', 'u.usr_id', '=', '_bp_usuarios_roles.usrls_usr_id')
                    ->select('_bp_usuarios_roles.usrls_id', 'r.rls_id', 'u.usr_usuario', 'r.rls_rol')
                    ->where('r.rls_estado', '<>', 'B')
                    ->where('u.usr_estado', '<>', 'B')
                    ->where('_bp_usuarios_roles.usrls_usr_id', $arr_user[0])
                    ->where('_bp_usuarios_roles.usrls_estado', '<>', 'B')
                    ->OrderBy('_bp_usuarios_roles.usrls_id', 'ASC')
                    ->get();
                    return $rolusuario;
    }

     public static function getUsuario($idusuario)
    {
         $rolusuario = RolUsuario::join('_bp_roles as r', 'r.rls_id', '=', '_bp_usuarios_roles.usrls_rls_id')
                    ->join('_bp_usuarios as u', 'u.usr_id', '=', '_bp_usuarios_roles.usrls_usr_id')
                    ->where('r.rls_estado', '<>', 'B')
                    ->where('u.usr_estado', '<>', 'B')
                    ->where('_bp_usuarios_roles.usrls_usr_id', $idusuario)
                    ->OrderBy('_bp_usuarios_roles.usrls_id', 'ASC')
                    ->get();
                    return $rolusuario;
    }

    protected static function getusuarios($idusuario,$rutaid){

        $gestrim = DB::select('select * from sp_obtiene_usuario(?,?)',array($idusuario,$rutaid));
        return $gestrim;
    }

    protected static function getRuta(){

        $gestrim = \DB::select('select * from sp_lst_ruta()');
        return $gestrim;
    }

    public static function lstVentas(){
        $ventas =\DB::select('select * from sp_lst_ventas()');
        return $ventas;
     }

     public static function getVentas($id){
        $ventas =\DB::select('select * from sp_get_ventas(?)',array($id));;
        return $ventas;
     }
    public static function lstDespachador(){
        $despachador = RolUsuario::join('_bp_usuarios', '_bp_usuarios.usr_id', '=','_bp_usuarios_roles.usrls_usr_id')
                              ->join('_bp_personas', '_bp_personas.prs_id','=' , '_bp_usuarios.usr_prs_id')
                              ->where('usrls_estado', 'A')
                              ->where('usrls_rls_id', '=',5)
                              ->where('_bp_personas.prs_estado', 'A')
                              ->where('_bp_usuarios.usr_estado', 'A')
                              ->select('_bp_personas.prs_id','_bp_personas.prs_nombres','_bp_personas.prs_paterno','_bp_personas.prs_materno')
                              ->OrderBy('_bp_usuarios_roles.usrls_id', 'ASC')
                              ->get();
        return $despachador;
     }

     protected static function listarOperador(){
        $usr = RolUsuario::join('_bp_usuarios', '_bp_usuarios.usr_id', '=','usrls_usr_id')
              ->join('_bp_personas', '_bp_personas.prs_id','=' , '_bp_usuarios.usr_prs_id')
              ->where('usrls_estado', 'A')
              ->where('usrls_rls_id', 32)
              ->where('_bp_personas.prs_estado', 'A')
              ->where('_bp_usuarios.usr_estado', 'A')
              ->select('_bp_personas.prs_id','_bp_personas.prs_nombres','_bp_personas.prs_paterno','_bp_personas.prs_materno')
              ->get();
        return $usr;
    }

    public static function getPersonas($id){
        $despachador = RolUsuario::join('_bp_usuarios', '_bp_usuarios.usr_id', '=','usrls_usr_id')
                              ->join('_bp_personas', '_bp_personas.prs_id','=' , '_bp_usuarios.usr_prs_id')
                              ->where('_bp_personas.prs_id', $id)
                              ->select('_bp_personas.prs_id','_bp_personas.prs_nombres','_bp_personas.prs_paterno','_bp_personas.prs_materno', 'prs_ci')
                              ->first();
        return $despachador;
     }

    protected static function getOperador($id){
        $usr = RolUsuario::join('_bp_usuarios', '_bp_usuarios.usr_id', '=','usrls_usr_id')
              ->join('_bp_personas', '_bp_personas.prs_id','=' , '_bp_usuarios.usr_prs_id')
              ->where('usrls_estado', 'A')
              ->where('_bp_personas.prs_id', $id)
              ->where('_bp_personas.prs_estado', 'A')
              ->where('_bp_usuarios.usr_estado', 'A')
              ->select('_bp_personas.prs_id','_bp_personas.prs_nombres','_bp_personas.prs_paterno','_bp_personas.prs_materno', '_bp_personas.prs_ci')
              ->first();
        return $usr;
    }
}
