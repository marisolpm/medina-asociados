<?php

namespace App\Modelo\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class AbogadoExpediente extends Model {

    protected $table = 'abogado_expediente';
    protected $fillable = ['id', 'id_expediente', 'id_empleado', 'fecha_asignacion', 'fecha_desvinculacion',
        'observacion', 'registro', 'usr_registro', 'modificado', 'usr_modificado', 'estado'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    protected static function ListaAbogadoExpediente($exp_id, $exp_id_abogado) {
        $abogadoExpediente = \DB::select('SELECT * FROM proc_expediente_abogado(?,?);', array($exp_id, $exp_id_abogado));
        return $abogadoExpediente;
    }
    
    protected static function RegistroAbogadoExpediente($tipo_registro,$id_expediente, $id_abogado) {
        $abogadoExpediente = \DB::select('SELECT * FROM proc_abogado_expediente_ins_upd(?,?,?,?)',
                        array($tipo_registro,$id_expediente, $id_abogado, Auth::user()->id));
        return $abogadoExpediente;
    }
    
    protected static function DesvincularAbogadoExpediente($tipo_registro,$id_expediente) {
        $abogadoExpediente = \DB::select('SELECT * FROM proc_abogado_expediente_ins_upd(?,?,?,?)',
                        array($tipo_registro,$id_expediente, -1, Auth::user()->id));
        return $abogadoExpediente;
    }

}
