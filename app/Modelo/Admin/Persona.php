<?php

namespace App\Modelo\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model {

    protected $table = 'personas';
    protected $fillable = ['id_tipo_persona', 'nombres', 'paterno', 'materno', 'ci', 'ci_expedido', 'celular', 'direccion', 'registrado', 'modificado',
        'usuario_reg', 'usuario_mod', 'area', 'estado', 'imagen'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function ListaPersonas($id_persona, $tipo_cliente) {
        $clientes = \DB::select('SELECT * FROM proc_persona_listado(?,?);', array($id_persona, $tipo_cliente));
        return $clientes;
    }

    protected static function RegistroPersonas($tipo_persona, $adjunto, $nombre, $paterno, $materno, $ci, $ci_expedido, $fecha_nacimiento, $email, $contacto, $direccion, $matricula, $area) {
        $clientes = \DB::select('SELECT * FROM proc_persona_ins_upd(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                        array(-1, $tipo_persona, $adjunto, $nombre, $paterno, $materno, $ci, $ci_expedido, $fecha_nacimiento, $email, $contacto, $direccion, Auth::user()->id, $matricula, $area));
        return $clientes;
    }

    protected static function ActualizarPersonas($tipo_persona, $id_persona, $adjunto, $nombre, $paterno, $materno, $ci, $ci_expedido, $fecha_nacimiento,
            $email, $contacto, $direccion, $matricula, $area) {
        $clientes = \DB::select('SELECT * FROM proc_persona_ins_upd(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                        array($id_persona, $tipo_persona, $adjunto, $nombre, $paterno, $materno, $ci, $ci_expedido, $fecha_nacimiento, $email, $contacto, $direccion, Auth::user()->id, $matricula, $area));
        return $clientes;
    }

    protected static function EliminarPersonas($id) {
        $clientes = \DB::select('UPDATE personas SET modificado = NOW(), usuario_mod = ?, estado = ?  WHERE id = ?', array(Auth::user()->id, 'B', $id));
        return $clientes;
    }

    protected static function ListaDepartamentos($id_departamento) {
        $departamentos = \DB::select('SELECT * FROM proc_departamento_listado(?);', array($id_departamento));
        return $departamentos;
    }

    protected static function ListaTipoProceso() {
        $tipoProceso = \DB::select('SELECT * FROM proc_tipo_proceso();');
        return $tipoProceso;
    }

}
