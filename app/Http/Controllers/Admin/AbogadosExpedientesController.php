<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\Admin\AbogadoExpediente;
use Yajra\Datatables\Datatables;

class AbogadosExpedientesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        foreach ($request->abogados as $id_abogado) {
            $respuesta = AbogadoExpediente::RegistroAbogadoExpediente(
                            $request->tipo_registro_abogado,
                            $request->expediente,
                            $id_abogado
            );
        }
        $collection = new \Illuminate\Support\Collection($respuesta[0]);
        return response()->json($collection);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $respuesta = AbogadoExpediente::DesvincularAbogadoExpediente(2, $id);
        $collection = new \Illuminate\Support\Collection($respuesta[0]);
        return response()->json($collection);
    }

    public function ListaAbogadosExpedientes($id_expediente, $id_empleado) {
        $abogadoExpedientes = AbogadoExpediente::ListaAbogadoExpediente($id_expediente, $id_empleado);
        return Datatables::of($abogadoExpedientes)
                        ->addColumn('o_asignacion', function ($abogadoExpedientes) {
                            return '<i class="fa fa-clock-o"> ' . $abogadoExpedientes->o_fecha_asignacion . '</i>'
                                    . '<i class="fa fa-user"> ' . $abogadoExpedientes->o_usuario_reg . '</i>';
                        })
                        ->addColumn('o_desvinculacion', function ($abogadoExpedientes) {
                            if ($abogadoExpedientes->o_estado === 'B')
                            return '<i class="fa fa-clock-o"> ' . $abogadoExpedientes->o_fecha_desvinculacion . '</i>'
                                    . '<i class="fa fa-user"> ' . $abogadoExpedientes->o_usuario_mod . '</i>';
                            else
                                return '---------------------';
                        })
                        ->addColumn('o_accion', function ($abogadoExpedientes) {
                            if ($abogadoExpedientes->o_estado === 'A')
                                return '<a class="btn btn-danger"  onclick="desvincularAbogado(' . $abogadoExpedientes->o_id . ')">'
                                        . '<i class="fa fa-delete"></i> DESVINCULAR</a>';
                            else
                                return 'DESVINCULADO';
                        })->rawColumns(['o_asignacion','o_desvinculacion', 'o_accion'])
                        ->make(true);
    }

}
