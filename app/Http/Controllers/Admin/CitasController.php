<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\Admin\Persona;
use App\Modelo\Admin\Cita;
use Yajra\Datatables\Datatables;
use App\User;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Auth;

class CitasController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Agenda';
        $_SESSION['sGrupoController'] = 'citas';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $thejson = null;
        $citas = \DB::select('SELECT * FROM citas WHERE id_empleado = ?;', array(Auth::user()->id));
        foreach ($citas as $event) {
            $thejson[] = array("title" => $event->titulo . "\n" . $event->mensaje, "description" => $event->mensaje, "start" => $event->fecha_hora_cita, "url" => $event->id);
        }
        return view('admin.citas.index', compact('thejson'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $respuesta = Cita::RegistroCita(
                        $request->tipo_registro,
                        $request->id_cita,
                        $request->cliente,
                        $request->empleado,
                        $request->asunto,
                        $request->lugar,
                        $request->estado_cita,
                        $request->fecha . " " . $request->hora,
                        $request->descripcion
        );

        return response()->json($respuesta[0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $respuesta = Cita::RegistroCita(
                        $request->tipo_registro,
                        $id,
                        $request->cliente,
                        $request->empleado,
                        $request->asunto,
                        $request->lugar,
                        $request->estado_cita,
                        $request->fecha . " " . $request->hora,
                        $request->descripcion
        );

        return response()->json($respuesta[0]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $cita = Cita::find($id)->delete();
        if ($cita) {
            $respuesta = array("retorno_codigo" => $id, "err_mensaje" => "SE ELIMINÓ CORRECTAMENTE");
            return response()->json($respuesta);
        }
    }

    public function ListaCitas($id_cita, $id_cliente) {
        $citas = Cita::ListaCitas($id_cita, $id_cliente, -1);
        return Datatables::of($citas)
                        ->addColumn('o_accion', function ($citas) {
                            return '
                                <a class="btn btn-primary" data-toggle="tooltip" title="Editar cita"  onclick="editarCita(' . $citas->o_id . ')"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger" data-toggle="tooltip" title="Eliminar cita"  onclick="eliminarCita(' . $citas->o_id . ')"><i class="fa fa-trash-o"></i></a>                    
';
                        })->rawColumns(['o_accion'])
                        ->make(true);
    }

    public function ListaCitasCalendario($id_cita, $id_cliente) {
        $citas = Cita::ListaCitas($id_cita, $id_cliente, -1);
        foreach ($citas as $event) {
            $thejson[] = array(
                "title" => "CITA CON \n" . $event->o_nombre_cliente . "\n" . $event->o_lugar_cita,
                "description" => $event->o_descripcion,
                "start" => $event->o_fecha_cita . " " . $event->o_hora_cita,
                "id_cita" => $event->o_id,
                "id_cliente" => $event->o_id_cliente,
                "id_abogado" => $event->o_id_abogado,
                "lugar" => $event->o_lugar_cita,
                "estado" => $event->o_estado_cita,
                "asunto" => $event->o_asunto,
            );
        }
        return $thejson;
    }

}
