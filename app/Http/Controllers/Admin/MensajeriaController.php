<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\Admin\Mensajeria;
use App\Modelo\Admin\Persona;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Mail;

class MensajeriaController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Plantillas e Integración';
        $_SESSION['sGrupoController'] = 'mensajeria';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $empleados = Persona::ListaPersonas(-1, 1);
        return view('admin.mensajeria.index', compact('empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $asunto = $request->asunto;
        $mensaje = $request->mensaje;
        $clienteN = '';
        $abogadoN = '';
        $enviarMail = '';
        $cliente = 0;

        $destinoArray = array();
        if ($request->cliente != 0 && !empty($request->cliente)) {
            $cliente = $request->cliente;
            $clienteD = Persona::ListaPersonas($cliente, -1);
            $clienteN = 'Sr(a): ' . $clienteD[0]->o_prs_nombre_completo;
            $clienteEmail = $clienteD[0]->o_prs_email;
            array_push($destinoArray, $clienteEmail);
        }
        $abogado = 0;
        if ($request->abogado != 0 && !empty($request->abogado)) {
            $abogado = $request->abogado;
            $abogadoD = Persona::ListaPersonas($abogado, -1);
            $abogadoN = 'Sr(a): ' . $abogadoD[0]->o_prs_nombre_completo;
            $abogadoEmail = $abogadoD[0]->o_prs_email;
            array_push($destinoArray, $abogadoEmail);
        }
        //dd($destinoArray);

        $destinoCC = ',';
        $url_upload = '';
        $data = array('clienteN' => $clienteN, "abogadoN" => $abogadoN, 'asunto' => $asunto, "mensaje" => $mensaje);
        $sendMail = Mail::send('email.mail', $data, function ($message) use ($destinoArray, $destinoCC, $url_upload, $asunto) {
                    $message->to($destinoArray)->subject($asunto);
                    $message->from('asociadosmedina07@gmail.com', 'Medina & Asociados');
//                    $destinoCCM = explode(',', $destinoCC);
//                    foreach ($destinoCCM as $dCC) {
//                        $message->addCC($dCC);
//                    }
//                    $message->attach($url_upload);
                });

        $respuesta = Mensajeria::RegistroMensaje(
                        1,
                        $cliente,
                        $abogado,
                        $asunto,
                        $mensaje
        );

        return response()->json($respuesta[0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $mensajeL = Mensajeria::ListaMensajes($id, -1, -1);

        $asunto = $mensajeL[0]->o_asunto;
        $mensaje = $mensajeL[0]->o_mensaje;
        $clienteN = '';
        $abogadoN = '';
        $enviarMail = '';
        $cliente = 0;

        $destinoArray = array();
        if ($mensajeL[0]->o_id_cliente != 0) {
            $cliente = $mensajeL[0]->o_id_cliente;
            $clienteD = Persona::ListaPersonas($cliente, -1);
            $clienteN = 'Sr(a): ' . $clienteD[0]->o_prs_nombre_completo;
            $clienteEmail = $clienteD[0]->o_prs_email;
            array_push($destinoArray, $clienteEmail);
        }
        $abogado = 0;
        if ($mensajeL[0]->o_id_abogado != 0) {
            $abogado = $mensajeL[0]->o_id_abogado;
            $abogadoD = Persona::ListaPersonas($abogado, -1);
            $abogadoN = 'Sr(a): ' . $abogadoD[0]->o_prs_nombre_completo;
            $abogadoEmail = $abogadoD[0]->o_prs_email;
            array_push($destinoArray, $abogadoEmail);
        }
        //dd($destinoArray);

        $destinoCC = ',';
        $url_upload = '';
        $data = array('clienteN' => $clienteN, "abogadoN" => $abogadoN, 'asunto' => $asunto, "mensaje" => $mensaje);
        $sendMail = Mail::send('email.mail', $data, function ($message) use ($destinoArray, $destinoCC, $url_upload, $asunto) {
                    $message->to($destinoArray)->subject($asunto);
                    $message->from('asociadosmedina07@gmail.com', 'Medina & Asociados');
//                    $destinoCCM = explode(',', $destinoCC);
//                    foreach ($destinoCCM as $dCC) {
//                        $message->addCC($dCC);
//                    }
//                    $message->attach($url_upload);
                });

        $respuesta = array("retorno_codigo" => $asunto, "err_mensaje" => "SE REENVIÓ CORRECTAMENTE");

        return response()->json($respuesta);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $mensaje = Mensajeria::find($id)->delete();
        if ($mensaje) {
            $respuesta = array("retorno_codigo" => $id, "err_mensaje" => "SE ELIMINÓ CORRECTAMENTE");
            return response()->json($respuesta);
        }
    }

    public function ListaMensajes($id_mensaje, $id_cliente, $id_abogado) {
        $mensaje = Mensajeria::ListaMensajes($id_mensaje, $id_cliente, $id_abogado);
        return Datatables::of($mensaje)
                        ->addColumn('o_accion', function ($mensaje) {
                            return '
                                <a class="btn btn-primary" data-toggle="tooltip" title="Reenviar mensaje"  onclick="reenviarMensaje(' . $mensaje->o_id . ')"><i class="fa fa-share-square-o"></i></a> 
                                <a class="btn btn-danger" data-toggle="tooltip" title="Eliminar mensaje"  onclick="eliminarMensaje(' . $mensaje->o_id . ')"><i class="fa fa-trash"></i></a>';
                        })->rawColumns(['o_accion'])
                        ->make(true);
    }

}
