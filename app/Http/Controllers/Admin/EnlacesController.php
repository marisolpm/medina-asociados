<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\Admin\Enlace;
use App\Modelo\Admin\Persona;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Mail;

class EnlacesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Plantillas e Integración';
        $_SESSION['sGrupoController'] = 'enlaces';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.enlaces.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $respuesta = Enlace::RegistroEnlace(
                        $request->enlace,
                        strtoupper($request->nombre_enlace),
                        $request->descripcion_enlace,
                        -1
        );
        return response()->json($respuesta);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $respuesta = Enlace::RegistroEnlace(
                        $request->enlace,
                        strtoupper($request->nombre_enlace),
                        $request->descripcion_enlace,
                        $id
        );
        return response()->json($respuesta);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function ListaEnlaces($id_enlace, $id_usuario, $estado) {
        $enlace = Enlace::ListaEnlaces($id_enlace, $id_usuario, $estado);
        return Datatables::of($enlace)
                        ->addColumn('o_accion_enlace', function ($enlace) {
                            return '
                                <a class="btn btn-default" data-toggle="tooltip" href="' . $enlace->o_enlace . '" target = "_blank" title="Abrir enlace" ><i class="fa fa-edit"> ' . $enlace->o_enlace . '</i></a>';
                        })
                        ->addColumn('o_accion', function ($enlace) {
                            return '
                                <a class="btn btn-primary" data-toggle="tooltip" title="Reenviar mensaje"  onclick="editarEnlace(' . $enlace->o_id . ')"><i class="fa fa-edit"></i></a>';
                        })->rawColumns(['o_accion_enlace', 'o_accion'])
                        ->make(true);
    }

}
