<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class MenuController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function links($var) {
        $vb = Auth::user()->id;

        $links = \DB::select('SELECT 
acceso.opciones.id, 
acceso.opciones.opcion, 
acceso.opciones.contenido,
acceso.opciones.grp_id,
acceso.grupos.grupo
FROM acceso.opciones

JOIN acceso.grupos ON acceso.opciones.grp_id =acceso.grupos.id
JOIN acceso.accesos ON acceso.opciones.id = acceso.accesos.opc_id
WHERE acceso.opciones.estado = ? AND
acceso.accesos.rls_id IN  ( SELECT acceso.usuarios_roles.rls_id FROM acceso.usuarios_roles
                            WHERE acceso.usuarios_roles.usr_id = ? AND
                            	acceso.usuarios_roles.estado = ?
                        )
AND acceso.opciones.grp_id = ?
                         AND acceso.accesos.estado = ? ORDER BY acceso.opciones.orden ASC', array('A', $vb, 'A', $var, 'A'));
        //dd($links);
        return $links;
    }

    public function submenus() {

        $valor1 = Auth::user()->id;
        $submenus = \DB::select('SELECT DISTINCT acceso.grupos.id, 
 acceso.grupos.grupo,
acceso.grupos.imagen
FROM acceso.grupos
JOIN acceso.opciones ON acceso.opciones.grp_id=acceso.grupos.id
JOIN acceso.accesos ON acceso.accesos.opc_id = acceso.opciones.id
JOIN acceso.roles ON acceso.roles.id =acceso.accesos.rls_id
JOIN acceso.usuarios_roles ON acceso.usuarios_roles.rls_id =acceso.accesos.rls_id
WHERE acceso.grupos.estado = ? AND
acceso.accesos.estado = ? AND
acceso.usuarios_roles.estado = ? AND
acceso.usuarios_roles.usr_id =  ?', array('A', 'A', 'A', $valor1));
        //dd($submenus);
        return $submenus;
    }

    public function imagenPersona() {
        $vb = Auth::user()->id;

        $links = \DB::select('SELECT 
                                p.imagen
                                FROM personas p
                                JOIN users u ON p.id = u.id_persona
                                WHERE u.id = ?', array($vb));
        return $links[0];
        
    }

    public function lenguaje() {
        $sProcessing = "Procesando Registros de la base de datos...";
        $sLengthMenu = "Mostrar _MENU_ registros";
        $sZeroRecords = "No se encontraron registros en la base de datos";
        $sInfo = "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros";
        $InfoEmply = "Mostrando registros del 0 al 0 de un total de 0 registros";
        $sInfoFiltered = "(filtrado de un total de _MAX_ registros)";
        $sInfoPostFix = "";
        $sSearch = "Buscar:";
        $sUrl = "";
        $sInfoThousands = ",";
        $sLoadingRecords = "Cargando...";
        $sFirst = "Primero";
        $Last = "Último";
        $Next = "Siguiente";
        $Previous = "Anterior";
        $sSortAscending = "Activar para ordenar la columna de manera ascendente";
        $sSortDescending = " Activar para ordenar la columna de manera descendente";
        return response()->json(["sProcessing" => $sProcessing, "sLengthMenu" => $sLengthMenu, "sZeroRecords" => $sZeroRecords, "sInfo" => $sInfo, "InfoEmply" => $InfoEmply, "sInfoFiltered" => $sInfoFiltered, "sInfoPostFix" => $sInfoPostFix, "sSearch" => $sSearch, "sUrl" => $sUrl, "sInfoThousands" => $sInfoThousands, "sLoadingRecords" => $sLoadingRecords, "oPaginate" => ["sFirst" => $sFirst, "sLast" => $Last, "sNext" => $Next, "sPrevious" => $Previous], "oAria" => ["sSortAscending" => $sSortAscending, "sSortDescending" => $sSortDescending]]);
    }

}
