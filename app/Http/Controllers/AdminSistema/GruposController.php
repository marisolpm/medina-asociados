<?php

namespace App\Http\Controllers\AdminSistema;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\AdminSistema\Grupo;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;

class GruposController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Administración Sistema';
        $_SESSION['sGrupoController'] = 'grupos';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.sistema.grupos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $ids = Auth::user()->id;
        $grupo = Grupo::create([
                    'grupo' => $request->grupo,
                    'imagen' => $request->imagen,
                    'usr_id' => $ids
        ]);
        return response()->json($grupo->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $grupo = Grupo::where('id', $id)->first();
        return response()->json($grupo->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $ids = Auth::user()->id;
      
        $grupo = Grupo::where('id', $id)->first();
        $resultado = $grupo;
        $grupo->grupo = $request->grupo;
        $grupo->imagen = $request->imagen;
        $grupo->modificado = date('Y-m-d H:i:s');
        $grupo->usr_id = $ids;
        $grupo->save();
        return response()->json($grupo->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $grupo = Grupo::where('id', $id)->update(['estado' => 'B']);
        return response()->json(["Mensaje" =>$grupo]); 
    }

    public function listado() {
        $grupos = Grupo::getListar();
        $grupos = new \Illuminate\Support\Collection($grupos);
        return Datatables::of($grupos)
                        ->addColumn('acciones', function ($grupo) {
                            return '<div class="btn-group">
                                        <a class="btn btn-primary" style="margin-left:12px;" title="Modificar" onClick="Mostrar(' . $grupo->id . ');"  data-target="#myUpdate"><i class="fa fa-edit"></i> Editar</a>
                                        <a class="btn btn-danger" style="margin-left:12px;" onClick="Eliminar(' . $grupo->id . ');" data-toggle="tooltip" title="Eliminar"><i class="fa fa-trash"></i> Eliminar</a>                                      
                                    </div>';
                        })->rawColumns(['acciones'])
                        ->make(true);
    }

}
