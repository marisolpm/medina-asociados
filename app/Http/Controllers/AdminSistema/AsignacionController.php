<?php

namespace App\Http\Controllers\AdminSistema;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelo\AdminSistema\Grupo;
use App\Modelo\AdminSistema\Acceso;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use Session;

class AsignacionController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Administración Sistema';
        $_SESSION['sGrupoController'] = 'accesos';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rol = Acceso::getListarRol();
        $opc = Acceso::getListarOpcionParam(13);
        $acceso = Acceso::getListarAccesoParam(13);
        return view('admin.gbAsignacion.index', compact('rol', 'opc', 'acceso'));
    }

    public function create() {
        $acceso = Acceso::getListar();
        return Datatables::of($acceso)
                        ->addColumn('asignacion', function ($acceso) {
                            return '
                    <input id="' . $acceso->acc_id . '" name="asignacion[]" tabindex="1" type="checkbox" value="' . $acceso->acc_id . '">';
                        })
                        ->make(true);
    }

    public function show() {
        $opc = Acceso::getListarOpcion();
        return Datatables::of($opc)
                        ->addColumn('opciones', function ($acceso) {
                            return '
                    <input id="' . $acceso->opc_id . '" name="opciones[]" tabindex="1" type="checkbox" value="' . $acceso->opc_id . '">';
                        })
                        ->make(true);
    }

    public function listaAc($id_r) {

        $acceso = Acceso::getListarAccesoParam($id_r);
        //$acc = new Collection($acceso);
        return Datatables::of($acceso)
                        ->addColumn('asignacion', function ($acceso) {
                            return '
                    <input id="' . $acceso->acc_id . '" name="asignacion[]" tabindex="1" type="checkbox" value="' . $acceso->acc_id . '">';
                        })
                        ->rawColumns(['asignacion'])
                        ->make(true);
    }

    public function listaOp($id_r) {
        $opc = Acceso::getListarOpcionParam($id_r);
        //$op = new Collection($opc);
        return Datatables::of($opc)
                        ->addColumn('opciones', function ($opc) {
                            return '
                    <input id="' . $opc->opc_id . '" name="opciones[]" tabindex="1" type="checkbox" value="' . $opc->opc_id . '">';
                        })
                        ->rawColumns(['opciones'])
                        ->make(true);
    }

    public function store(Request $request) {
        
    }

    public function actualiza1() {

        $ids = Auth::user()->id;
        $id_rol = $_POST['id_rol'];
        
        if (isset($_POST['opciones'])) {
            $arr_opc = $_POST['opciones'];
            for ($i = 0; $i < count($arr_opc); $i++) {
                Acceso::create([
                    'rls_id' => $id_rol,
                    'opc_id' => $arr_opc[$i],
                    'usr_id' => $ids
                ]);
            }
        }
    }

    public function actualiza2() {
        $ids = Auth::user()->id;
        if (isset($_POST['asignacion'])) {
            $arr_acc = $_POST['asignacion'];

            for ($i = 0; $i < count($arr_acc); $i++) {
                Acceso::where('id', $arr_acc[$i])
                        ->update(['estado' => 'B']);
            }
        }
    }

    public function edit($id) {
        $grupo = Grupo::setBuscar($id);
        $grupillo = Grupo::getListar();
        return view('admin.gbGrupos.update', compact('grupo', 'grupillo'));
    }

    public function update(Request $request, $id) {
        $grupo = Grupo::setBuscar($id);
        $grupo->fill($request->all());
        $grupo->save();
        $grupillo = Grupo::getListar();
        Session::flash('message', 'El grupo se editado Correctamente');
        return view('admin.gbGrupos.read', compact('grupillo'));
    }

    public function destroy($id) {
        
    }

}
