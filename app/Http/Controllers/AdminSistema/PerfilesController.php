<?php

namespace App\Http\Controllers\AdminSistema;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Modelo\Admin\Persona;
use App\Modelo\AdminSistema\Usuario;
use App\Resize;
use FilesystemIterator;
use Illuminate\Support\Facades\Auth;

class PerfilesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $_SESSION['grupoController'] = 'Gestión Personas';
        $_SESSION['sGrupoController'] = 'perfil';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $departamentos = Persona::ListaDepartamentos(-1);
        $areas = Persona::ListaTipoProceso();
        return view('admin.sistema.perfil.index', compact('departamentos', 'areas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $usr = Usuario::getListar();
        return datatables::of($usr)->addColumn('acciones', function ($usuario) {
                            return '<div class="btn-group">
                                        <a class="btn btn-default"  href="/clientes/' . $usuario->id . '"><i class="fa fa-keyboard-o"></i> Resetear acceso</a>
                                        <a class="btn btn-primary"  style="margin-left:12px;" href="/clientes/' . $usuario->id . '/edit"><i class="fa fa-edit"></i> Editar</a>
                                        <a class="btn btn-danger" style="margin-left:20px;" href="admin/clients/delete/' . $usuario->id . '" onclick="return areyousure()"><i class="fa fa-trash"></i> Eliminar</a>
                                    </div>';
                        })
                        ->rawColumns(['acciones'])
                        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $persona = Persona::ListaPersonas($id, -1);
        return response()->json($persona);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $contrasena = \DB::select('SELECT * FROM proc_cambiar_contraeña(?,?,?,?)', array($id, Auth::user()->id, $request->password, 2));
        return response()->json($contrasena);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function subirFoto(Request $request) {
        $prs_id = $request->get('prs_id');
        $imagen_anterior = Persona::find($prs_id);
        $prs_imagen = '';

        if (isset($_FILES['prs_imagen']) &&
                $_FILES['prs_imagen']['tmp_name'] != '') {
            $nombre = $prs_id . '_' . date('YmdHis') . '.jpg';
            $url = '/persona/foto/' . $nombre;
            $url_upload = $_SERVER['DOCUMENT_ROOT'] . '/persona/foto/' . $nombre;
            $destination_extension = strtolower(pathinfo($_FILES['prs_imagen']['tmp_name'], PATHINFO_EXTENSION));

            if (exif_imagetype($_FILES['prs_imagen']['tmp_name']) === IMAGETYPE_JPEG) {
                $imagenOriginal = $_FILES['prs_imagen']['tmp_name'];
                $exif = exif_read_data($_FILES['prs_imagen']['tmp_name']);
                if (!empty($exif) && isset($exif['Orientation'])) {
                    $ort = $exif['Orientation'];
                    //dd($ort);
                    switch ($ort) {
                        case 1:
                            $angle = 0;
                            break;
                        case 2:
                            $angle = 0;
                            break;
                        case 3:
                            $angle = 180;
                            break;
                        case 4:
                            $angle = 0;
                            break;
                        case 5:
                            $angle = -90;
                            break;
                        case 6:
                            $angle = 270;
                            break;
                        case 7:
                            $angle = -90;
                            break;
                        case 8:
                            $angle = 90;
                            break;
                    }

                    $imgInfo = getimagesize($imagenOriginal);
                    $mime = $imgInfo['mime'];
                    switch ($mime) {
                        case 'image/jpeg':
                            $original = @imagecreatefromjpeg($imagenOriginal);
                            break;
                        case 'image/png':
                            $original = @imagecreatefrompng($imagenOriginal);
                            break;
                        case 'image/gif':
                            $original = @imagecreatefromgif($imagenOriginal);
                            break;
                        default:
                            $original = @imagecreatefromjpeg($imagenOriginal);
                    }
                    if (!$original) {
                        $original = @imagecreatefromstring($imagenOriginal);
                    }
                    $rtOriginal = @imagerotate($original, $angle, 0);
                } else {
                    $rtOriginal = @imagecreatefromjpeg($imagenOriginal);
                }
                @imagejpeg($rtOriginal, $url_upload);
                @imagedestroy($rtOriginal);
                $resizeObj = new Resize($url_upload);
            } else {
                // Creamos una imagen
                $imagenOriginal = $_FILES['prs_imagen']['tmp_name'];
                $imgInfo = getimagesize($imagenOriginal);
                $mime = $imgInfo['mime'];
                switch ($mime) {
                    case 'image/jpeg':
                        $original = @imagecreatefromjpeg($imagenOriginal);
                        break;
                    case 'image/png':
                        $original = @imagecreatefrompng($imagenOriginal);
                        break;
                    case 'image/gif':
                        $original = @imagecreatefromgif($imagenOriginal);
                        break;
                    default:
                        $original = @imagecreatefromjpeg($imagenOriginal);
                }
                if (!$original) {
                    $original = @imagecreatefromstring($imagenOriginal);
                }
                ////Ancho y alto máximo
                $max_ancho = 500;
                $max_alto = 500;

//Medir la imagen
                list($ancho, $alto) = getimagesize($imagenOriginal);

//Ratio
                $x_ratio = $max_ancho / $ancho;
                $y_ratio = $max_alto / $alto;

//Proporciones
                if (($ancho <= $max_ancho) && ($alto <= $max_alto)) {
                    $ancho_final = $ancho;
                    $alto_final = $alto;
                } else if (($x_ratio * $alto) < $max_alto) {
                    $alto_final = ceil($x_ratio * $alto);
                    $ancho_final = $max_ancho;
                } else {
                    $ancho_final = ceil($y_ratio * $ancho);
                    $alto_final = $max_alto;
                }
//
//Crear un lienzo
                $lienzo = @imagecreatetruecolor($ancho_final, $alto_final);

//Copiar original en lienzo
                @imagecopyresampled($lienzo, $original, 0, 0, 0, 0,
                                $ancho_final, $alto_final, $ancho, $alto);

//Destruir la original
                @imagedestroy($original);

//Crear la imagen y guardar en directorio upload/
                @imagejpeg($lienzo, $url_upload, 30);
                $resizeObj = new Resize($url_upload);
            }
        }


        //$imagenAnteriorObj = new

        $resizeObj->resizeImage(150, 150, 'crop');
        $resizeObj->saveImage($url_upload, null, 100);

        $id = $prs_id;
        if ($imagen_anterior->imagen != '/assets/img/avatar5.png' && is_file($_SERVER['DOCUMENT_ROOT'] . $imagen_anterior->imagen)) {
            $imagen_anterior = $_SERVER['DOCUMENT_ROOT'] . $imagen_anterior->imagen;
            unlink($imagen_anterior);
        }
        $per = Persona::find($id);
        if ($_FILES) {
            $per->imagen = $url;
            $per->usuario_mod = Auth::user()->id;
            $per->modificado = date('Y-m-d H:i:s');
        }

        $per->save();
        return response()->json($per->toArray());
    }

    public function rotarFoto() {
        $imagen = Persona::find($_POST['prs_id']);
        switch ($_POST['tipo_rotacion']) {
            case 1:
                $angle = 90;
                break;
            case 2:
                $angle = -90;
                break;
            case 3:
                $angle = 180;
                break;
        }

        $origen = @imagecreatefromjpeg($_SERVER['DOCUMENT_ROOT'] . $imagen->imagen);

// Rotar
        $rotar = @imagerotate($origen, $angle, 0);

// Imprimir
        @imagejpeg($rotar, $_SERVER['DOCUMENT_ROOT'] . $imagen->imagen, 100);
        //imagejpeg($rotar,$_SERVER['DOCUMENT_ROOT'] . $imagen->prs_imagen,100);
// Liberar la memoria
        imagedestroy($origen);
        imagedestroy($rotar);
        return response()->json($imagen->toArray());
    }

}
